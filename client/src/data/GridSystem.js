import Heap from "./Heap";
import CustomerNode from "./CustomerNode"
import DriverNode from "./DriverNode"


export default class GridSystem {
  constructor(width, height, columns, rows, totalCustomers, totalDrivers) {
    this.grid = new Map();
    this.processedCustomers = 0;
    this.processedDrivers = 0;
    this.setTotalCustomers(totalCustomers);
    this.setTotalDrivers(totalDrivers);
    this.calculteGrid(width, height, columns, rows);
  }
  calculteGrid(width, height, columns, rows) {
    this.width = width;
    this.height = height;
    this.gridWidth = this.width / columns;
    this.gridHeight = this.width / rows;
    this.center = {x:this.width * 0.5,y:this.height * 0.5}
    for (let i = 0; i < columns; i++) {
      for (let j = 0; j < rows; j++) {
        this.createGrid(i, j)
      }
    }
    this.computeUniformDistribution();
  }
  setTotalCustomers(totalCustomers){
    this.totalCustomers = totalCustomers;
  }
  setTotalDrivers(totalDrivers){
    this.totalDrivers = totalDrivers;
  }
  getGridXY(i,j){
    return {
      x:j*this.gridWidth,
      y:i*this.gridHeight
    }
  }

  getGridCenterXY(i,j){
    let gridXY = this.getGridXY(i,j);
    return {
      x:gridXY.x + this.gridWidth*0.5,
      y:gridXY.y + this.gridHeight*0.5
    }
  }
  getQuadrant(centerX,centerY,x,y){
    let radians = Math.atan2(y-centerY,x-centerX);
    if(radians >=0&&radians<=Math.PI*0.5)
      return 0;
    else if(radians>Math.PI*0.5&&radians<=Math.PI)
      return 1;
    else if(radians>=-Math.PI*0.5)
      return 2;
    else
      return 3;      
  }
  
  computeUniformDistribution(totalCustomers,totalDrivers){
    // let keys = [...this.grid.keys()];
    // let key,randomX,randomY,quadrant,quadCenX,quadCenY;
    // while(this.processedCustomers < this.totalCustomers || this.processedDrivers < this.totalDrivers){
    //   key=Math.floor(Math.random() * keys.length);
    //   randomX = this.grid.get(key).position.x + Math.random() * this.gridWidth;
    //   randomY = this.grid.get(key).position.y + Math.random() * this.gridHeight;
    //   quadCenX=this.grid.get(key).quadrants.center.x;
    //   quadCenY=this.grid.get(key).quadrants.center.y;
    //   quadrant = this.getQuadrant(quadCenX,quadCenY,randomX,randomY);
    //   //this.grid.get(key).quadrants.data[quadrant].get("customerHeap")
    //   //this.grid.get(key).quadrants.data[quadrant].get("driverHeap")

    //   this.processedCustomers++;
    //   this.processedDrivers++;
    // }
  }
  
  createGrid(i, j) {
    let key = i+','+j;
    let gridCenter = this.getGridCenterXY(i,j);
    let gridXY = this.getGridXY(i,j);
    this.grid.set(key,{
      position:{
        left:gridXY.x,
        top:gridXY.y
      },
      center:gridCenter,
      quadrants:{
        data:this.createQuadrants(),
        adjacents:[],
        matchedCustomers:[],
        matchedDrivers:[]
      }
    })
  }

  createSubGrid(index, totalGrids) {
    let subGrid = new Map();
    subGrid.set("centerX", 0);
    subGrid.set("centerY", 0);
    for (let j = 0; j < 4; j++) {
      subGrid.set("q_0" + j, this.createQuadrants(subGrid.get("centerX"), subGrid.get("centerY")));
    }
    return subGrid;
  }

  createQuadrants() {
    let quadHeap,customerHeap,driverHeap,quadrants=[];
    
    for(let i=0;i<4;i++){
      quadHeap = new Map();
      customerHeap = new Heap(1000);
      driverHeap = new Heap(1000);
      console.log("Customer Heap : ", customerHeap);
      let cusNode = new CustomerNode(20+i,20);
      let driverNode = new DriverNode(30+i,30);
      customerHeap.insert(cusNode);
      driverHeap.insert(driverNode);
      quadHeap.set("customerHeap", customerHeap);
      quadHeap.set("driverHeap", driverHeap);
      quadrants.push(quadHeap);
    }
    return quadrants;
  }

  // insertDriver(subGrid, quadrant, driverNode) {
  //   this.grid.get(subGrid);
  //   let quadrant = subGrid.get(quadrant);
  //   let driverHeap = quadrant.get("driverHeap");
  //   driverHeap.insert(driverNode);
  // }

  // insertCustomer(subGrid, quadrant, customerNode) {
  //   let subGrid = this.grid.get(subGrid);
  //   let quadrant = subGrid.get(quadrant);
  //   let customerHeap = quadrant.get("customerHeap");
  //   cusotmerHeap.insert(customerNode);
  // }

}