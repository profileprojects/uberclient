export const DriverState = Object.freeze({
    IDLE:   "idle",
    BUSY:  "busy",
    INACTIVE: "inactive"
});