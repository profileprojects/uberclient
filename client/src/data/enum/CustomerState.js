export const CustomerState = Object.freeze({
    WAITING:   "waiting",
    RIDE:  "ride",
    ABANDONED: "abandoned"
});