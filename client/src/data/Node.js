export default class Node{
    constructor(x, y, actor, state){
        this.x = x;
        this.y = y;
        this.actor = actor;
        this.state = state;
        this.distance = null;
    }

    setDistance(originX, originY){
        this.distance = Math.sqrt(Math.pow(originX - this.x, 2) + Math.pow(originY-this.y,2));
    }

}