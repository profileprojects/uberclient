import Node from "./Node";
import {DriverState} from "./enum/DriverState";
import {Actor} from "./enum/Actor";

export default class Driver extends Node{
    constructor(x,y){
        super(x,y);
        this.actor = Actor.DRIVER;
        this.state = DriverState.IDLE;
    }
}