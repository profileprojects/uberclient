import Node from "./Node";
import {CustomerState} from "./enum/CustomerState";
import {Actor} from "./enum/Actor";

export default class Customer extends Node{
    constructor(x,y){
        super(x,y);
        this.actor = Actor.CUSTOMER;
        this.state = CustomerState.WAITING;
    }
}