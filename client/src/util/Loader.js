export default class Loader{
  constructor(imageList, callback){
    this.imageList = imageList;
    this.images={};
  }
  loadImage(){
    if(this.imageList){
      let completedImages = 0;
      this.imageList.map((url)=>{
        let img = new Image();
        img.src = url;
        let name =  url.substring(url.lastIndexOf('/')+1);
        name = name.split('.')[0];
        this.images[name] = img;
        img.onload = (event) => {
          completedImages++;
          if(completedImages>=this.imageList.length){
            if(callback){
              callback(this.images);
            }
          }
        }
      })
    }
  }
}