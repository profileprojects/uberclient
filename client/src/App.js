import React from 'react';
// import logo from './logo.svg';
import './App.css';
// import Grid from "@material-ui/core/Grid";
// import { FormLabel, FormControl } from '@material-ui/core';
import { Container } from '@material-ui/core';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";


// Components
import Header from "./Components/Header/Header";
import MapArea from "./Components/MapArea/MapArea";
import Form from "./Components/Form/Form";
// import DSRating from "./Components/DSRating/DSRating";
import SystemDesign from "./Components/SystemDesign/SystemDesign";

class App extends React.Component {
  constructor(props){
    super(props);
    this.updateCustomerNDriver=this.updateCustomerNDriver.bind(this);
    this.state={
      data:{
        customer:2,
        driver:2
      }
    }
  }
  updateCustomerNDriver(data){
    this.setState({
      data:{
        customer:data.customer,
        driver:data.driver
      }
    })
  }
  render(){
    return (
      <div className="App">
        <Router>
          <Header />
          <Container>
            <Switch>
              <Route path="/systemDesign">
                <SystemDesign />
              </Route>
              <Route path="/aboutUs">
                About Us
              </Route>
              <Route path="/">
                <MapArea customerNDriverCountData={this.state.data}/>
                <Form updateCustomerNDriver={this.updateCustomerNDriver}/>
                {/* <Grid container spacing={2} justify="center" alignItems="center" margin-top={2}>
                  <Grid item xs={4}>
                    <FormControl className="bordered">
                      <FormLabel>Efficiency</FormLabel>
                      <DSRating></DSRating>
                    </FormControl>
                  </Grid>
                </Grid> */}
              </Route>
            </Switch>
  
          </Container>
        </Router>
      </div>
    );
  }
  
}

export default App;
