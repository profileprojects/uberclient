import React from "react";
import { Grid } from "@material-ui/core";

import "./SystemDesign.css";
export default class SystemDesign extends React.Component{
    onError(e){
        console.warn("Error in file viewer : ", e);
    }
    render(){
        return (
            <Grid container justify="center">
                <Grid item xs="8">
                <iframe title="System Design" src="https://docs.google.com/document/d/e/2PACX-1vRFF_300u-PvqdKCmr9Y_oN6QgQ0wU9w_BtixU3Z6jtbH46MlqoJhtfd26hThYrnuXjcExhPRiz18B_/pub?embedded=true" className="iframeBox"></iframe>
                </Grid>
            </Grid>
        );
    }
}