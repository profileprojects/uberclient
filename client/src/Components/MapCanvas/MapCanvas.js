import React from 'react';
import './MapCanvas.css'
export default class CanvasComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      animFrameId: null,
      circles: [],
      loadedImages: 0,
      assets: {}
    }
    this.updateCanvas = this.updateCanvas.bind(this);
  }
  componentDidMount() {
    this.loadImage();
  }
  generateRandomCircle() {
    let circles = [];
    let maxCircles = 0;
    let ctx = this.refs.canvas.getContext('2d');
    let x, y;
    for (let i = 0; i < maxCircles; i++) {
      x = Math.random() * ctx.canvas.width;
      y = Math.random() * ctx.canvas.height;
      circles.push({
        x,
        y
      })
    }
    this.setState({
      circles: circles
    })
  }
  loadImage() {
    let completedImages = 0;
    let assets = {};
    let imageList = {
      customerImage:this.props.customerImage,
      driverImage:this.props.driverImage,
      mapImage:this.props.mapImage
    }
    let keys = Object.keys(imageList);
    keys.map((key) => {
      let image = new Image();
      let url = imageList[key];
      image.src = url;
      // let name =  url.substring(url.lastIndexOf('/')+1);
      // name = name.split('.')[0];
      assets[key] = image;
      console.log('key',key);
      image.onload = (event) => {
        completedImages++;
        console.log("CC",completedImages)
        if (completedImages >= keys.length) {
          this.generateRandomCircle();
          let animFrameId = requestAnimationFrame(this.updateCanvas);
          this.setState({
            loadedImages: completedImages,
            assets: assets,
            animFrameId
          })
        }
      }
    })
  }
  updateCanvas() {
    const ctx = this.refs.canvas.getContext('2d');
    //this.scaleToFill(this.state.assets.mapImage, ctx, this.refs.canvas);
    //this.drawCircle();
    this.drawImage();
    let animFrameId = requestAnimationFrame(this.updateCanvas)
    this.setState({
      animFrameId: animFrameId
    });
  }
  drawImage(){
    const ctx = this.refs.canvas.getContext('2d');
    let img = this.state.assets.customerImage;
    this.state.circles.map((point) => {
      ctx.drawImage(img, 0, 0,img.width,img.height, point.x,point.y,5,5);
    })
   // this.scaleToFill(this.state.assets['driver'],ctx,this.refs.canvas)
  }
  drawCircle(x, y, radius) {
    const ctx = this.refs.canvas.getContext('2d');
    ctx.beginPath();
    this.state.circles.map((circle) => {
      ctx.moveTo(circle.x, circle.y);
      ctx.arc(circle.x, circle.y, 3, 0, 2 * Math.PI);
    })
    ctx.stroke();
  }
  scaleToFill(img, ctx, canvas) {
    // get the scale
    var scale = Math.max(canvas.width / img.width, canvas.height / img.height);
    // get the top left position of the image
    var x = (canvas.width / 2) - (img.width / 2) * scale;
    var y = (canvas.height / 2) - (img.height / 2) * scale;
    ctx.drawImage(img, x, y, img.width * scale, img.height * scale);
  }
  scaleToFit(img, ctx, canvas) {
    // get the scale
    var scale = Math.min(canvas.width / img.width, canvas.height / img.height);
    // get the top left position of the image
    var x = (canvas.width / 2) - (img.width / 2) * scale;
    var y = (canvas.height / 2) - (img.height / 2) * scale;
    ctx.drawImage(img, x, y, img.width * scale, img.height * scale);
  }
  makeCircles() {
    var x, y,
      circle,
      div = document.querySelector('#circles');

    for (x = 1; x <= 100; x++) {
      for (y = 1; y <= 100; y++) {
        circle = document.createElement('div');
        circle.className = 'circle';
        div.appendChild(circle);
      }
    }
  } //makeCircles
  render() {
    return ( 
      //<div className='container'>test</div>
      <canvas ref = "canvas" className = {this.props.className} / >
    );
  }

}