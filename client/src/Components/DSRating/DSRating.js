import React from 'react';
import PropTypes from 'prop-types';
import Rating from '@material-ui/lab/Rating';
import Tooltip from '@material-ui/core/Tooltip';
import Box from '@material-ui/core/Box';

const labels = {
  0.5: 'All Customers abandoned',
  1: 'Almost all Customers abandoned',
  1.5: 'Rarely Customers are matched',
  2: 'Few Customers are matched',
  2.5: 'Average Matching ',
  3: 'Some Customers are matched',
  3.5: 'Good',
  4: 'Most Customers are matched',
  4.5: 'Almost all Customers are matched',
  5: 'All Customers matched',
};

function IconContainer(props) {
  const { value, ...other } = props;
  return (
    <Tooltip title={labels[value] || ''}>
      <span {...other} />
    </Tooltip>
  );
}

IconContainer.propTypes = {
  value: PropTypes.number.isRequired,
};

export default function DSRating() {
  const value = 2;

  return (
    <div>
      <Box component="fieldset" mb={3} borderColor="transparent">
        <Rating
          name="hover-tooltip"
          value={value}
          precision={0.5}
          IconContainerComponent={IconContainer}
        />
      </Box>
    </div>
  );
}