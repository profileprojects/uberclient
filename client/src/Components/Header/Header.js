import React from 'react';
// import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
// import IconButton from '@material-ui/core/IconButton';
import { Grid } from '@material-ui/core';
import {
  Link
} from "react-router-dom";

import "./Header.css";

export default function Header() {


  return (
    <div className="header">
      <AppBar position="static">
        <Toolbar>
          <Grid container alignItems="center">
            <Grid item xs="6">
              <Link to="/" className="titleLink">
                <Typography variant="h6" className="title">
                  Uber Demand Supply Simulation
              </Typography>
              </Link>
            </Grid>
            <Grid item xs="6">
              <Grid container justify="flex-end" alignItems="center">
                <Grid item xs="4">
                  <Link to="/systemDesign" className="titleLink">
                    <Typography variant="h8" className="title">
                      System Design
                    </Typography>
                  </Link>
                </Grid>
                <Grid item xs="4">
                  <Link to="/aboutUs" className="titleLink">
                    <Typography variant="h8" className="title">
                      About Us
                    </Typography>
                  </Link>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
    </div>
  );
}