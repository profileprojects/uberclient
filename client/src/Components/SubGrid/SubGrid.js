import React from "react";
import Quadrant from "../Quadrant/Quadrant";

export default class SubGrid extends React.Component{
    constructor(props){
        super(props);
        this.state = {};
    }
    componentDidMount(){
    }
    
    renderQuadrant(){
      return this.props.subgrid.quadrants.data.map((quadrant, index) => {
            return <Quadrant 
                      currentWidth={this.props.currentWidth} 
                      currentHeight={this.props.currentHeight} 
                      refWidth={this.props.refWidth} 
                      refHeight={this.props.refHeight} 
                      type={this.props.type} 
                      quadrant={quadrant} 
                      key={index}
                      filter={this.props.filter}
                    /> 
        })
    }
    render(){
        return(
            <>
                {this.renderQuadrant()}
            </>
        );
    }
}