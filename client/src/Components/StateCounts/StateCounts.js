import React from 'react';
import { Container, Grid, Chip, FormGroup, FormControlLabel } from '@material-ui/core';
import Switch from '@material-ui/core/Switch';
import "./StateCounts.css";

export default function StateCounts(props) {
  const handleDelete = (data) => {
    props.handleToggleFilter(props.type, data, false);
  };
  const filterAllToggle = () =>{
    // console.log("Triggering all check",props.filter[props.type], props.type,props.filter);
    for(let key in props.filter){
      // console.log("key : ", key, " filter  : ", props.filter, " value : ", props.filter[key]);
      if(!props.filter[key])
        return false;
    }
    return true;
  }
  const renderIcon = (color) => {
    return (
      <svg width="100" height="100">
        <circle cx="50%" cy="50%" r="5" fill={color} />
      </svg>
    );
  }

  const calculateAllCount = () => {
    // console.log("CalcualteAllCOunts : ", props.counts);
    if(!props.counts)
      return 0;
    // console.log("Counts in calculate: ", props.counts);
    let count =0;
    for(let key of props.states){
      // console.log("state count ", props.counts,props.counts[key])
      count += props.counts[key];
    }
    return count;
  }
  return (
    <Container className="stateFilterContainer">
        <Grid container justify="center" alignItems="center" direction="row" spacing={3} className="backgroundFilter">
          <Grid item xs={3}>
            <FormGroup>
              <FormControlLabel
                control={<Switch 
                          size="small" 
                          onChange={() => {props.handleAllFilter(props.type)}} 
                          checked={filterAllToggle()}
                        />}
                label={"All ["+ calculateAllCount() +"]"} 
                labelPlacement="start"
              />
            </FormGroup>

          </Grid>
          <Grid item xs={9}>
            <Grid container justify="flex-end">
              {props.states.map((data, index) => {
                let icon = renderIcon(props.filterColors[index]);

                return (
                  <Grid item key={data} xs={4}>
                    <Chip
                      icon={icon}
                      label={data + " ["+props.counts[data]+"]"}
                      onDelete={() => {handleDelete(data)}}
                      className="chip"
                      size="small"
                      disabled={!props.filter[data]}
                      clickable
                    />
                  </Grid>
                  
                );
              })}

            </Grid>

          </Grid>
        </Grid>
    </Container>);
}