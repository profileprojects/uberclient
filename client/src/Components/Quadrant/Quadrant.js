import React from "react";

export default class Quadrant extends React.Component{
    constructor(props){
        super(props);
        this.state= {};
    }
    convertHeapToPoints(){
      return this.props.type === 'customer' ? this.props.quadrant['customerHeap'] : this.props.quadrant['driverHeap']; 
    }
    componentDidUpdate(){

    }
    renderCustomerPoints(){

      let points = this.props.quadrant['customerHeap'].arr;
      if(!this.props.filter.customer.waiting)
        return;
      return (
      <>
        {points.map((point,index)=>{
          return <circle cx={this.scaleX(point.x)} cy={this.scaleY(point.y)} r={this.scaleX(5)} fill="orange" key={index}/>
        })}
      </>
      )
    }
    
    renderMatchedCustomerPoints(){
      //console.log("Matched Customers : ", this.props.quadrant['matchedCustomers'].length);
      if(!this.props.quadrant['matchedCustomers'])
        return;
      let points = this.props.quadrant['matchedCustomers'];
        if(!this.props.filter.customer.matched)
          return;
      return (
      <>
        {points.map((point,index)=>{
          return <circle cx={this.scaleX(point.x)} cy={this.scaleY(point.y)} r={this.scaleX(5)} fill="green" key={index}/>
        })}
      </>
      )
    }

    scaleX(x){
      return (x/this.props.refWidth) * this.props.currentWidth;
    }
    scaleY(y){
      return (y/this.props.refHeight) * this.props.currentHeight;
    }
    renderDriverPoints(){
      let points = this.props.quadrant['driverHeap'].arr;
      if(!this.props.filter.driver.idle)
        return;
      return (
      <>
        {points.map((point,index)=>{
          return <circle cx={this.scaleX(point.x)} cy={this.scaleY(point.y)} r={this.scaleX(5)} fill="orange" key={index}/>
        })}
      </>
      )
    }
    
    renderMatchedDriverPoints(){
      if(!this.props.quadrant['matchedDrivers'])
        return;
      let points = this.props.quadrant['matchedDrivers'];
      if(!this.props.filter.driver.matched)
        return;
      return (
      <>
        {points.map((point,index)=>{
          return <circle cx={this.scaleX(point.x)} cy={this.scaleY(point.y)} r={this.scaleX(5)} fill="green" key={index}/>
        })}
      </>
      )
    }
    render(){

      if(this.props.type === 'customer'){
        return(
          <>
            {this.renderCustomerPoints()}
            {this.renderMatchedCustomerPoints()}
          </>
        );
      }
      else{
        return(
          <>
            {this.renderDriverPoints()}
            {this.renderMatchedDriverPoints()}
          </>
        );
      }
        // return(
        //     <circle cx={10} cy={10} r={2} fill="red" />
        // );
    }
}