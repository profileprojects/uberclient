/* eslint import/no-webpack-loader-syntax: off */
import React from 'react';
import { Grid, Container } from '@material-ui/core';
import "./MapArea.css";
import openSocket from 'socket.io-client';
// Components
import Map from "../Map/Map";

// // web worker
// import gridWorker from "../../gridWorker";
// import WebWorker from "../../workerSetup";

export default class MapArea extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      gridSystem : null,
      refWidth:500,
      refHeight:400,
      width:0,
      height:0
    }
  }
  customerMap(element){
    //console.log('element',element);
  }
  updateDimensions(){
    //console.log('resize_check',this.refs.customerMap.clientWidth);
    this.setState({
      width:this.refs.customerMap.clientWidth,
      height:this.refs.customerMap.clientHeight
    })
  }
  componentWillReceiveProps(nextProps){
    //console.log('componentDidUpdate',nextProps,this.socket)
    if(nextProps.customerNDriverCountData){
      this.socket.emit('addCustomersNDrivers',{data:nextProps.customerNDriverCountData})  
    }
    
    //this.socket.emit('addCustomerNDriver',{data:this.props.customerNDriverCountData})
  }
  componentDidMount(){
    // console.log('ref_map',this.refs.customerMap.clientWidth);
    window.addEventListener('resize',this.updateDimensions.bind(this))
    this.setState({
      width:this.refs.customerMap.clientWidth,
      height:this.refs.customerMap.clientHeight,
      refWidth:500,
      refHeight:400
    })
    this.socket = openSocket("ws://localhost:3010/");

    this.socket.on("connection", (data) => {
      // console.log("Connection initialization of data : ", data);
    });
    
    this.socket.on('userConnected', res => {
      //console.log("Data from web server : ", res);
      this.setState({
        refWidth:res.data.refWidth,
        refHeight:res.data.refHeight
      })
      //this.setState({gridSystem: res.data.gridSystem});
    })

    this.socket.on('entry',res=>{
      // console.log('got entry algo from socket');
    //  console.log("entry count : ", res.data.counts);
      this.setState({gridSystem:res.data.gridSystem, counts: res.data.counts});
    })

    this.socket.on('match',res=>{
      //console.log('%c got match algo from socket! ', ' color: red');
      //console.log('got match',res.key,res.data.gridSystem,res.data.counts)
    //  console.log("match count : ", res.data.counts);
      this.setState({gridSystem:res.data.gridSystem, counts: res.data.counts});
    })

    this.socket.on('closure',res=>{
      //console.log('got closure',res.key,res.data.gridSystem,res.data.counts)
      //console.log('%c got closure algo from socket! ', ' color: blue');
    //  console.log("closure count : ", res.data.counts);
      this.setState({gridSystem:res.data.gridSystem, counts: res.data.counts});
    })
  }

  render() {  
    return (
      <Container className="root">
        <Grid container spacing={2} justify="center" alignItems="center">
          <Grid item md xs={10}>
            <div className="mapContainer" ref="customerMap">
              <Map type="customer" gridSystem={this.state.gridSystem} refWidth={this.state.refWidth} refHeight={this.state.refHeight} currentWidth={this.state.width} currentHeight={this.state.height} counts={this.state.counts}/>
            </div>
          </Grid>
          <Grid item md xs={10}>
            <div className="mapContainer">
              <Map type="driver" gridSystem={this.state.gridSystem} refWidth={this.state.refWidth} refHeight={this.state.refHeight} currentWidth={this.state.width} currentHeight={this.state.height}  counts={this.state.counts}/>
            </div>
            
          </Grid>
        </Grid>
      </Container>
    );

  }
}