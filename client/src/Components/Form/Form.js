import React from 'react';
import Grid from "@material-ui/core/Grid";
// import { makeStyles } from '@material-ui/core/styles';
import Fab from '@material-ui/core/Fab';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import LocalTaxiIcon from '@material-ui/icons/LocalTaxi';
import EmojiPeopleIcon from '@material-ui/icons/EmojiPeople';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import {Container} from '@material-ui/core';
import "./Form.css";

// const useStyles = makeStyles(theme => ({
//     root: {
//         flexGrow: 1,
//     },
//     fab: {
//         margin: theme.spacing(2),
//         "font-size": 14
//     },
//     extendedIcon: {
//         marginRight: theme.spacing(1),
//     },
//     Grid: {
//         spacing: theme.spacing(2),
//     },
//     textField: {
//         marginLeft: theme.spacing(1),
//         marginRight: theme.spacing(1),
//         width: 200,
//     },
// }));

export default class Form extends React.Component {
  constructor(props)
  {
    super(props);
    this.updateCustomerCount  = this.updateCustomerCount.bind(this);
    this.updateDriverCount = this.updateDriverCount.bind(this);
    this.addCustomerNDriver = this.addCustomerNDriver.bind(this);
    this.state = {
      customer:2,
      driver:2
    }
    this.customerInput=null;
    this.driverInput=null
  }
  isInValidInputs(){
    return this.customerInput.value === "" || this.driverInput.value === "" || this.state.customer === "" || this.state.driver === "";
  }
  addCustomerNDriver(){
    if(this.isInValidInputs())
      return;
    //console.log("addCustomerNDriver"); 
    let data = {
      customer:parseInt(this.state.customer),
      driver:parseInt(this.state.driver)
    }  
    this.props.updateCustomerNDriver(data);
  }
  updateCustomerCount(e){
    // if(e.target.value==="")
    //   return;
    //console.log('customer count',e.target.value);
    this.setState({
      customer:e.target.value
    })
  }
  updateDriverCount(e){
    // if(e.target.value==="")
    //   return;
    //console.log('driver count',e.target.value);
    this.setState({
      driver:e.target.value
    })
  }
  componentDidMount(){
    //this.addCustomerNDriver();
  }
  render()
  {
    return (
        <Container color="text.primary">
            <Grid container spacing={2} justify="center" alignItems="center" direction="row">
                <Grid item xs={6} md>
                    <TextField
                        inputRef={el => this.customerInput = el}
                        id="filled-number input-with-icon-textfield"
                        label="Customer"
                        type="number"
                        className="textField"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        value={this.state.customer}
                        onChange={this.updateCustomerCount}
                        margin="normal"
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <EmojiPeopleIcon />
                                </InputAdornment>
                            ),
                        }}
                    />
                </Grid>
                <Grid item xs={6} md>

                    <TextField
                        inputRef={el => this.driverInput = el}
                        id="filled-number input-with-icon-textfield"
                        label="Driver"
                        type="number"
                        className="textField"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        value={this.state.driver}
                        onChange={this.updateDriverCount}
                        margin="normal"
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <LocalTaxiIcon />
                                </InputAdornment>
                            ),
                        }}
                    />
                </Grid>
                <Grid item xs={5} md>
                    <Fab variant="extended" aria-label="like" className="fab" color="primary" onClick={this.addCustomerNDriver}>
                        <PersonAddIcon className="extendedIcon" />
                        Add
                    </Fab>
                </Grid>

            </Grid>

        </Container>
    );
  }  
}