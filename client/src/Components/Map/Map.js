import React from 'react';
import {Card} from '@material-ui/core';
import "./Map.css";
// Components
import StateCounts from "../StateCounts/StateCounts";
import SubGrid from "../SubGrid/SubGrid";
import _ from "lodash";

import EmojiPeopleIcon from '@material-ui/icons/EmojiPeople';
import LocalTaxiIcon from '@material-ui/icons/LocalTaxi';

const states = {
  customer: ['waiting', 'matched'],
  driver: ['idle','matched']
};
const filterColors = {
  customer: ['orange', 'green'],
  driver: ['orange','green']
};
export default class Map extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      points:[],
      gridSystem: null,
      filter: {
        customer:{
          waiting: true,
          matched: true
        },
        driver: {
          idle: true,
          matched: true
        }
      }
    }
    this.ref = React.createRef();
    this.toggleFilter = this.toggleFilter.bind(this);
    this.enableAllFilters = this.enableAllFilters.bind(this); 
    // console.log("Grid System : ", this.state.gridSystem);
  }
  updatePoints(){
    let maxCircles = 500;
    let points=[];
    for(let i=0;i<maxCircles;i++){
     points.push({x:Math.random()*580,y:Math.random()*450,color:"rebeccapurple"}) 
    }
    this.setState({points})
  }
  componentDidMount(){
    this.updatePoints();
    // console.log("refs : ", this.ref);
    setInterval(()=>{
      if(!this.ref.current || !this.ref.current.classList)
        return;
      if(this.ref.current.classList.contains("shadow")){
        this.ref.current.classList.remove("shadow");
      }else{
        this.ref.current.classList.add("shadow");
      }
    },2000)
  }

  enableAllFilters(actor){
    // console.log("enable all filters : ",actor);
    const filter = _.clone(this.state.filter);
    if(actor === 'customer'){
      filter['customer'] = {
        waiting: true,
        matched: true
      };
    }else{
      filter['driver'] = {
        idle: true,
        matched: true
      };
    }
    this.setState({filter});
  }
  toggleFilter(actor, state, bool){
    const filter = _.clone(this.state.filter);
    filter[actor][state] = bool;
    this.setState({filter});
    // console.log("Filter status : ", filter);
  }
  renderSubGrid(){
    if(!this.props.gridSystem)
      return;
    // console.log("Grid System : ", this.props.gridSystem);
    return [...Object.keys(this.props.gridSystem)].map(key => {
      return <SubGrid 
                currentWidth={this.props.currentWidth} 
                currentHeight={this.props.currentHeight} 
                refWidth={this.props.refWidth} 
                refHeight={this.props.refHeight} 
                type={this.props.type} 
                subgrid={this.props.gridSystem[key]} 
                filter={this.state.filter}
                key={key}
              />;
    });
  }
  renderFilter(){
    
    // console.log("Counts : ", this.props.counts);
    if(!this.props.counts)
      return;

    if(this.props.type === 'customer'){
      return (
        <>
          <StateCounts 
            states={states.customer} 
            handleAllFilter= {this.enableAllFilters} 
            handleToggleFilter={this.toggleFilter}
            type={this.props.type}
            filter={this.state.filter[this.props.type]}
            filterColors={filterColors.customer}
            counts={this.props.counts.customer}
          />
        </>
      );
    }else{
      return (
        <>
          <StateCounts 
            states={states.driver} 
            handleAllFilter= {this.enableAllFilters} 
            handleToggleFilter={this.toggleFilter} 
            type={this.props.type}
            filter={this.state.filter[this.props.type]}
            className="filters"
            filterColors={filterColors.driver}
            counts={this.props.counts.driver}
          />
        </>
      );
    }
  }
  renderTitle(title){
    if(this.props.type === "customer")
    return (
      <div className="mapTitle">
          <EmojiPeopleIcon style={{ fontSize: 45, color: 'blue'}}/>
      </div>
    );

    return (
      <div className="mapTitle">
          <LocalTaxiIcon style={{ fontSize: 45, color: 'blue'}}/>
      </div>
    );
  }
  render(){
    return (
        <Card className="card img-overlay-wrap shadow" ref={this.ref}>         
          {/* <div className="img-overlay-wrap"> */}
            {this.renderTitle(this.props.type)}
            <img src="map.jpg" alt="map"/>
            <svg id="points">
              {this.renderSubGrid()}
            </svg>
            {this.renderFilter()}
          {/* </div> */}
        </Card>
    )
  };
}