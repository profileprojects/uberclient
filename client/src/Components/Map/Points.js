import React from 'react';
export default function Points(props){
  const scaleX = (x) => {
    return (x/props.refWidth) * props.currentWidth;
  }
  const scaleY = (y) => {
    return (y/props.refHeight) * props.currentHeight;
  }
  return (
    <div className="img-overlay-wrap">
      <img src="map.jpg"/>
      <svg>
        {/* {this.renderCircle()} */}
        {props.data.map((point,index) => {
          return <circle cx={scaleX(point.x)} cy={scaleY(point.y)} r={2} fill={point.color} key={index}/>
        })}
      </svg>
    </div>
  )
}