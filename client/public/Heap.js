import Node from "./Node";

export default class Heap {
    constructor(max) {
        this.max = max;
        this.size = 0;
        this.arr = [];
    }
    getParentIndex(i) {
        return Math.floor(i / 2);
    }

    getLeftChildIndex(i) {
        return 2 * i + 1;
    }

    getRightChildIndex(i) {
        return 2 * i + 2;
    }
    heapifyUp(i) {
        let parentIndex, parent, node;
        while (i > 0) {
            parentIndex = this.getParentIndex(i);
            parent = this.arr[parentIndex];
            node = this.arr[i];
            if (parent.distance < node.distance)
                break;
            this.arr[parentIndex] = node;
            this.arr[i] = parent;
            i = parentIndex;
        }
    }

    heapifyDown(i){
        let leftIndex, rightIndex, leftChild, rightChild, node, left;
        while(i<this.size){
            node = this.arr[i];
            leftIndex = this.getLeftChildIndex(i);
            left = this.arr[leftIndex];
            if(left && left.distance < node.distance){
                this.arr[leftIndex] = node;
                this.arr[i] = leftChild;
                i = leftIndex;
                continue;
            }
            rightIndex = this.getRightChildIndex(i);
            rightChild = this.arr[rightIndex];
            if(rightChild && rightChild.distance < node.distance){
                this.arr[rightIndex] = node;
                this.arr[i] = rightChild;
                i = rightIndex;
                continue;
            }
            break;
        }
    }
    
    insert(node) {
        this.arr.push(node);
        this.size++;
        this.heapifyUp(this.size - 1);
    }

    extract(){
        const root = this.arr[0];
        this.size--;
        const last = this.arr.pop();
        this.arr[0] = last;
        this.heapifyDown(0);
    }

}